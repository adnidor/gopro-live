#!/usr/bin/python3
import main
import tkinter as tk
import tkinter.ttk as ttk
import threading
from time import sleep

stop_flag = False

def ka_loop():
    global stop_flag
    while True:
        print(stop_flag)
        if stop_flag:
            return
        main.send_keepalive()
        sleep(main.KEEPALIVE_INTERVAL/1000)

class MainWindow(tk.Tk):
    def __init__(self):
        super().__init__()
        

        self.frame = tk.Frame(self)
        self.frame.pack()

        resolution_frame = tk.Frame(self.frame)
        resolution_frame.pack()

        resolution_label = tk.Label(resolution_frame, text="Auflösung: ")
        resolution_label.pack(side=tk.LEFT)

        resolution_selector = ttk.Combobox(resolution_frame)
        resolution_selector.configure(values = list(main.RESOLUTIONS.keys()))
        resolution_selector.configure(state = "readonly")
        resolution_selector.current(0)
        resolution_selector.pack(side=tk.LEFT)

        resolution_button = tk.Button(resolution_frame, text="Übernehmen", command=lambda: self.on_resolution_change(resolution_selector))
        resolution_button.pack(side=tk.LEFT)



        bitrate_frame = tk.Frame(self.frame)
        bitrate_frame.pack()

        bitrate_label = tk.Label(bitrate_frame, text="Bitrate: ")
        bitrate_label.pack(side=tk.LEFT)

        bitrate_selector = ttk.Combobox(bitrate_frame)
        bitrate_selector.configure(values=["1000000", "1500000", "2000000", "2500000", "3000000"])
        bitrate_selector.pack(side=tk.LEFT)

        bitrate_button = tk.Button(bitrate_frame, text="Übernehmen", command=lambda: self.on_bitrate_change(bitrate_selector))
        bitrate_button.pack(side=tk.LEFT)

        start_button = tk.Button(self.frame, text="Start", command=self.start)
        start_button.pack()

        #stop_button = Gtk.Button(label="Stop")
        #stop_button.connect("clicked", self.stop)
        #self.box.add(stop_button)

    def start(self):
        main.start_stream()
        main.start_ffplay()
        main.send_keepalive()
        self.ka_thread = threading.Thread(target=ka_loop)
        self.ka_thread.start()

    def exit(self, abcd=None):
        self.stop()
        self.destroy()

    def stop(self, button=None):
        try:
            if self.ka_thread.is_alive():
                global stop_flag
                stop_flag = True
                self.ka_thread.join(3)
        except AttributeError:
            pass
        main.stop_ffplay()

    def on_bitrate_change(self, selector):
        try:
            bitrate = int(selector["values"][selector.current()])
            print(bitrate)
            main.set_bitrate(bitrate)
        except ValueError:
            print("ValueError")
            selector.set_active(-1)
        pass

    def on_resolution_change(self, selector):
        resolution = selector["values"][selector.current()]
        print(resolution)
        main.set_resolution(resolution)
        pass
    

def on_destroy(window):
    Gtk.main_quit()

win = MainWindow()
win.protocol("WM_DELETE_WINDOW", win.exit)
win.mainloop()
