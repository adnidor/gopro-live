#!/usr/bin/python3
import subprocess
from requests import get
from socket import socket,AF_INET,SOCK_DGRAM
import logging
from time import sleep

BITRATE_URL = "http://10.5.5.9/gp/gpControl/setting/62/"
RESOLUTION_URL = "http://10.5.5.9/gp/gpControl/setting/64/"
STREAM_START_URL = "http://10.5.5.9/gp/gpControl/execute?p1=gpStream&a1=proto_v2&c1=restart"
ZOOM_URL = "http://10.5.5.9/gp/gpControl/command/digital_zoom?range_pcnt="

#tested
RESOLUTIONS = { "432x240": "1",
                "336x240": "2",
                "224x240": "3",
                "864x480": "4",
                "640x480": "5",
                "432x480": "6",
                "1280x720": "7",
                "960x720": "8",
                "640x720": "9" }

KEEPALIVE_INTERVAL = 2500
KEEPALIVE_MESSAGE = b"_GPHD_:%u:%u:%d:%1lf\n" % (0, 0, 2, 0)
KEEPALIVE_DEST = ("10.5.5.9", 8554)

FFPLAY_COMMAND = ["ffplay", '-i', '-f:v', 'mpegts', 'udp://10.5.5.100:8554', '-fflags', 'nobuffer', '-probesize', '8192']

ffplay_process = None

def start_stream():
    get(STREAM_START_URL)

def send_keepalive():
    s = socket(AF_INET,SOCK_DGRAM)
    s.sendto(KEEPALIVE_MESSAGE,KEEPALIVE_DEST)

def start_ffplay():
    global ffplay_process
    if ffplay_process is not None:
        logging.error("ffplay already running")
        return
    ffplay_process = subprocess.Popen(FFPLAY_COMMAND)

def stop_ffplay():
    global ffplay_process
    if ffplay_process is None:
        logging.error("ffplay not running")
        return
    ffplay_process.terminate()
    ffplay_process.wait()
    ffplay_process = None

def set_resolution(resolution):
    get(RESOLUTION_URL+RESOLUTIONS[resolution])

def set_bitrate(bitrate):
    get(BITRATE_URL+str(int(bitrate)))

def set_zoom(zoom):
    get(ZOOM_URL+str(int(zoom)))

def keepalive_loop():
    while True:
        sleep(KEEPALIVE_INTERVAL/1000)
        send_keepalive()
