#!/usr/bin/python3
import main
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import threading
from time import sleep

stop_flag = False

def ka_loop():
    global stop_flag
    while True:
        if stop_flag:
            raise Exception
        main.send_keepalive()
        sleep(main.KEEPALIVE_INTERVAL)

class MainWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="GoPro Streaming")
        self.header = Gtk.HeaderBar()
        self.header.set_title("GoPro Streaming")
        self.set_titlebar(self.header)

        self.box = Gtk.Box(orientation="vertical", spacing=10)
        self.add(self.box)

        resolution_box = Gtk.Box(spacing=4)

        resolution_label = Gtk.Label(label="Auflösung: ")
        resolution_box.add(resolution_label)

        resolution_selector = Gtk.ComboBoxText()
        resolution_box.add(resolution_selector)
        for key,value in main.RESOLUTIONS.items():
            resolution_selector.append_text(key)
        resolution_selector.set_active(0)

        resolution_button = Gtk.Button(label="Übernehmen")
        resolution_button.connect("clicked", self.on_resolution_change, resolution_selector)
        resolution_box.add(resolution_button)

        self.box.add(resolution_box)


        bitrate_box = Gtk.Box(spacing=4)

        bitrate_label = Gtk.Label(label="Bitrate: ")
        bitrate_box.add(bitrate_label)

        bitrate_selector = Gtk.ComboBoxText.new_with_entry()
        bitrate_box.add(bitrate_selector)
        bitrate_selector.append_text("1000000")
        bitrate_selector.append_text("1500000")
        bitrate_selector.append_text("2000000")
        bitrate_selector.append_text("2500000")
        bitrate_selector.append_text("3000000")

        bitrate_button = Gtk.Button(label="Übernehmen")
        bitrate_button.connect("clicked", self.on_bitrate_change, bitrate_selector)
        bitrate_box.add(bitrate_button)

        self.box.add(bitrate_box)

        zoom_box = Gtk.Box(orientation="horizontal")

        zoom_label = Gtk.Label(label="Zoom")
        zoom_box.add(zoom_label)

        zoom_scale = Gtk.Scale.new_with_range(Gtk.Orientation(0),0,100,10)
        zoom_scale.set_digits(0)
        zoom_scale.set_hexpand(True)
        zoom_box.add(zoom_scale)

        zoom_button = Gtk.Button(label="Übernehmen")
        zoom_button.connect("clicked", self.on_zoom_change, zoom_scale)
        zoom_box.add(zoom_button)

        self.box.add(zoom_box)

        start_button = Gtk.Button(label="Start")
        start_button.connect("clicked", self.start)
        self.box.add(start_button)

        stop_button = Gtk.Button(label="Stop")
        stop_button.connect("clicked", self.stop)
        self.box.add(stop_button)

    def start(self, button):
        main.start_stream()
        main.start_ffplay()
        main.send_keepalive()
        self.ka_thread = threading.Thread(target=ka_loop)
        self.ka_thread.start()

    def exit(self, abcd=None):
        self.stop()
        Gtk.main_quit()

    def stop(self, button=None):
        try:
            if self.ka_thread.is_alive():
                global stop_flag
                stop_flag = True
                self.ka_thread.join(3)
        except AttributeError:
            pass
        main.stop_ffplay()

    def on_zoom_change(self, button, scale):
        main.set_zoom(int(scale.get_value()))
        print(scale.get_value())
        pass

    def on_bitrate_change(self, button, selector):
        try:
            bitrate = int(selector.get_active_text())
            print(bitrate)
            main.set_bitrate(bitrate)
        except ValueError:
            print("ValueError")
            selector.set_active(-1)
        pass

    def on_resolution_change(self, button, selector):
        resolution = selector.get_active_text()
        main.set_resolution(resolution)
        print(resolution)
        pass
    

def on_destroy(window):
    Gtk.main_quit()

win = MainWindow()
win.connect("destroy",win.exit)
win.show_all()
Gtk.main()
